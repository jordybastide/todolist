<?php

namespace App;

use PDO;

class MySQL extends PDO
{
    /**
     * Instance de la classe PDO
     *
     * @var PDO
     * @access private
     */
    private $PDOInstance = null;

    /**
     * Instance de la classe MySQL
     *
     * @var MySQL
     * @access private
     * @static
     */
    private static $instance = null;

    /**
     * Constante: nom d'utilisateur de la bdd
     *
     * @var string
     */
    const DEFAULT_SQL_USER = 'root';

    /**
     * Constante: hôte de la bdd
     *
     * @var string
     */
    const DEFAULT_SQL_HOST = '127.0.0.1';

    /**
     * Constante: port de la bdd
     *
     * @var string
     */
    const DEFAULT_SQL_PORT = '3307';

    /**
     * Constante: pw de la bdd
     *
     * @var string
     */
    const DEFAULT_SQL_PASS = '';

    /**
     * Constante: nom de la bdd
     *
     * @var string
     */
    const DEFAULT_SQL_DTB = 'todo';

    /**
     * Constructeur
     *
     * @param void
     * @return void
     * @see PDO::__construct()
     * @access private
     */
    public function __construct()
    {
        parent::__construct('mysql:host='.self::DEFAULT_SQL_HOST.';port='. self::DEFAULT_SQL_PORT.';dbname='.self::DEFAULT_SQL_DTB, self::DEFAULT_SQL_USER, self::DEFAULT_SQL_PASS);
        $this->PDOInstance = new PDO('mysql:host='.self::DEFAULT_SQL_HOST.';port='. self::DEFAULT_SQL_PORT.';dbname='.self::DEFAULT_SQL_DTB, self::DEFAULT_SQL_USER, self::DEFAULT_SQL_PASS);
        echo "MySQL Database Connection" . PHP_EOL;
    }

    /**
     * Crée et retourne l'objet MySQL
     *
     * @access public
     * @static
     * @param void
     * @return MySQL $instance
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new MySQL();
        }
        return self::$instance;
    }

}
