<?php

namespace App\Controller;

use App\Model\Task;
use App\Controller\StorageInterface;

class TaskController
{
    protected $storage;

    public function __construct(StorageInterface $storage)
    {
        $this->storage = $storage;
    }

    public function addTask(Task $task)
    {
        return $this->storage->store($task);
    }

    public function updateTask(Task $task)
    {
        return $this->storage->update($task);
    }

    public function getTask($id)
    {
        return $this->storage->get($id);
    }

    public function deleteTask($id)
    {
        return $this->storage->delete($id);
    }

    public function getTasks()
    {
        return $this->storage->all();
    }
}
