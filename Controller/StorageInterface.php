<?php

namespace App\Controller;

use App\Model\Task;

interface StorageInterface
{
    public function store(Task $task);
    public function update(Task $task);
    public function delete($id);
    public function get($id);
    public function all();
}
