<?php

namespace App\Controller;

use PDO;
use App\MySQL;
use App\Model\Task;
use App\Controller\StorageInterface;

class StorageController implements StorageInterface
{
    protected $db;

    public function __construct(MySQL $db)
    {
        $this->db = $db;
    }

    public function store(Task $task)
    {
        $statement = $this->db->prepare("
				INSERT INTO task (description)
				VALUES (:description)
				");

        $statement->execute($this->buildcolumns($task));

        return $this->get($this->db->lastInsertId());
    }

    public function update(Task $task)
    {
        $statement = $this->db->prepare("
				UPDATE task SET description = :description WHERE id = :id
				");

        $statement->execute($this->buildcolumns($task, ['id' => $task->getid(),]));
        return $this->get($task->getid());
    }

    //delete
    public function delete($id)
    {
        $statement = $this->db->prepare("DELETE FROM task WHERE id = :id");
        $statement->execute(['id' => $id,]);
        return $statement->fetch();
    }

    public function get($id)
    {
        $statement = $this->db->prepare("SELECT id, description FROM task WHERE id = :id
				");

        $statement->setFetchMode(PDO::FETCH_CLASS, Task::class);

        $statement->execute([
            'id' => $id,
        ]);
        return $statement->fetch();
    }

    public function all()
    {
        $statement = $this->db->prepare("
				SELECT id, description FROM task order by id desc
				");

        $statement->setFetchMode(PDO::FETCH_CLASS, Task::class);

        $statement->execute();

        return $statement->fetchAll();
    }

    protected function buildcolumns(Task $task, array $additional =[])
    {
        return array_merge([
            'description' => $task->getdescription(),
        ], $additional);
    }
}
