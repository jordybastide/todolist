<?php

namespace App;

use App\MySQL;
use App\PostgreSQL;

class DBFactory
{

    public static function getConn($dbtype) {

        switch($dbtype) {
            case "MySQL":
                $dbobj = new MySQL();
//                var_dump(spl_object_id($dbobj));
                break;
            case "pgSQL":
                $dbobj = new PostgreSQL();
                break;
            default:
                $dbobj = new MySQL();
                break;
        }

        return $dbobj;
    }

}
